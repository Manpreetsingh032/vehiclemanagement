package `in`.his.vehiclemanagement.Fragment

import `in`.his.vehiclemanagement.Dashboard
import `in`.his.vehiclemanagement.R
import `in`.his.vehiclemanagement.Signature
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.driving_details.*
import java.text.SimpleDateFormat
import java.util.*


class DrivingDetails : Fragment() {
    private var cal = Calendar.getInstance()
    var dob = ""
    lateinit var c: Context

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.driving_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        submit_by_sec.setOnClickListener {
            Toast.makeText(requireContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show()
            parentFragmentManager.beginTransaction()
                .replace(R.id.container_main, Dashboard.newInstance()).commitAllowingStateLoss()
        }

        check_by_security_sign.setOnClickListener {
            Signature().callFunction(c)
        }

        date_tv_cal.setOnClickListener {
            displayDate()
        }

    }
    private fun displayDate() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        val a =
            DatePickerDialog(
                c,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
        a.datePicker.maxDate = System.currentTimeMillis()
        a.show()

    }

    private fun updateDateInView() {
        val myFormat = "dd MMMM yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        date_tv!!.text = sdf.format(cal.time)
        dob = date_tv?.text.toString()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }

    companion object {

        fun newInstance() =
            DrivingDetails()
    }
}