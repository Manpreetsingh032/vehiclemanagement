package `in`.his.vehiclemanagement.Fragment

import `in`.his.vehiclemanagement.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class SearchDriverDetails : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.search_driver_details, container, false)
    }

    companion object {

        fun newInstance() =
            SearchDriverDetails()
    }
}