package `in`.his.vehiclemanagement.Fragment

import `in`.his.vehiclemanagement.R
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.driver_details.*
import java.text.SimpleDateFormat
import java.util.*

class DriverDetails : Fragment() {
    private var cal = Calendar.getInstance()
    var dob = ""
    lateinit var c: Context

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.driver_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        age_calendar.setOnClickListener {
            displayDate()
        }

        submit.setOnClickListener {
            if (first_name.text.isEmpty() ||
                last_name.text.isEmpty() ||
                mobile_no.text.isEmpty() ||
                age.text.isEmpty() ||
                address.text.isEmpty() ||
                driving_lic.text.isEmpty() ||
                truck_no.text.isEmpty() ||
                rc_no.text.isEmpty()
            ) {
                if (first_name.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Name", Toast.LENGTH_SHORT).show()
                } else if (last_name.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Name", Toast.LENGTH_SHORT).show()
                } else if (mobile_no.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Mobile No", Toast.LENGTH_SHORT).show()
                } else if (age.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Age", Toast.LENGTH_SHORT).show()
                } else if (address.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Address", Toast.LENGTH_SHORT).show()
                } else if (driving_lic.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Driving License", Toast.LENGTH_SHORT)
                        .show()
                } else if (truck_no.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add Truck No", Toast.LENGTH_SHORT).show()
                } else if (rc_no.text.isEmpty()) {
                    Toast.makeText(requireContext(), "Add RC No", Toast.LENGTH_SHORT).show()
                }
            } else {
                driverProfile()
            }
        }

    }

    private fun driverProfile() {
        first_name.text.toString()
        last_name.text.toString()
        mobile_no.text.toString()
        dob
        address.text.toString()
        driving_lic.text.toString()
        truck_no.text.toString()
        rc_no.text.toString()
        Toast.makeText(requireContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show()
    }

    private fun displayDate() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        val a =
            DatePickerDialog(
                c,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
        a.datePicker.maxDate = System.currentTimeMillis()
        a.show()

    }

    private fun updateDateInView() {
        val myFormat = "dd MMMM yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        age!!.text = sdf.format(cal.time)
        dob = age?.text.toString()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }

    companion object {

        fun newInstance() =
            DriverDetails()
    }
}