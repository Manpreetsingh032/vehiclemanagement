package `in`.his.vehiclemanagement.Fragment

import `in`.his.vehiclemanagement.R
import `in`.his.vehiclemanagement.Signature
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.driver_details.*
import kotlinx.android.synthetic.main.inspection_list.*

class InspectionList : Fragment() {
    lateinit var c: Context
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.inspection_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loading_sup_sign.setOnClickListener {
            Signature().callFunction(c)
        }
            security_sign.setOnClickListener {
            Signature().callFunction(c)
        }
        driver . setOnClickListener {
            Signature().callFunction(c)
        }
        logistics_sign . setOnClickListener {
            Signature().callFunction(c)
        }

        submit_list.setOnClickListener {
            Toast.makeText(requireContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }

    companion object {

        fun newInstance() =
            InspectionList()
    }
}