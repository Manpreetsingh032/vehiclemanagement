package `in`.his.vehiclemanagement

import `in`.his.vehiclemanagement.Fragment.DriverDetails
import `in`.his.vehiclemanagement.Fragment.DrivingDetails
import `in`.his.vehiclemanagement.Fragment.InspectionList
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dashboard.*

class Dashboard : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        in_list.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(
                    R.id.container_main,
                    InspectionList.newInstance()
                )
                .commitAllowingStateLoss()
        }

        out_list.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(
                    R.id.container_main,
                    DrivingDetails.newInstance()
                )
                .commitAllowingStateLoss()
        }
    }

    companion object {

        fun newInstance() =
            Dashboard()
    }
}