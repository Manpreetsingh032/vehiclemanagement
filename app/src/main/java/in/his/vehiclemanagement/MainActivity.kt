package `in`.his.vehiclemanagement

import `in`.his.vehiclemanagement.Fragment.DriverDetails
import `in`.his.vehiclemanagement.Fragment.SearchDriverDetails
import android.app.Dialog
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.appbar.*
import kotlin.math.max
import kotlin.math.min

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container_main, Dashboard.newInstance()).commitAllowingStateLoss()

        home.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_main, Dashboard.newInstance()).commitAllowingStateLoss()
        }

        search_driver_detail.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container_main,
                    SearchDriverDetails.newInstance()
                )
                .commitAllowingStateLoss()
        }
        new_reg.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container_main,
                    DriverDetails.newInstance()
                )
                .commitAllowingStateLoss()
        }


    }



}