package `in`.his.vehiclemanagement

import android.app.Dialog
import android.content.Context
import android.graphics.*
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import kotlin.math.max
import kotlin.math.min

class Signature {
    private lateinit var sign_dialog: Dialog
    lateinit var mContent: LinearLayout
    var mSignature: Signature? = null
    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var view: View
    var sv = false
    var sa = false
    var bAuthSign = false
    var bVisSign = false
    var bitmap1: Bitmap? = null
    var bitmap2: Bitmap? = null



     fun callFunction(context:Context) {
        dialogAction(context)
    }

     private fun dialogAction(context:Context) {
        sign_dialog = Dialog(context)
        sign_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        sign_dialog.setContentView(R.layout.dialog_signature)
        sign_dialog.setCancelable(true)
        mContent = sign_dialog.findViewById(R.id.linearLayout)
        mSignature = Signature(context)
        mSignature!!.setBackgroundColor(Color.WHITE)
        mContent.addView(
            mSignature,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        mClear = sign_dialog.findViewById(R.id.clear)
        mGetSign = sign_dialog.findViewById(R.id.getsign)
        mGetSign.isEnabled = false
        mCancel = sign_dialog.findViewById(R.id.cancel)
        view = mContent

        mClear.setOnClickListener {
            Log.v("tag", "Panel Cleared")
            mSignature!!.clear()
            mGetSign.isEnabled = false
        }

        mGetSign.setOnClickListener {
            mSignature!!.save(view)
            sign_dialog.dismiss()
        }

        mCancel.setOnClickListener {
            Log.v("tag", "Panel Cancelled")
            sign_dialog.dismiss()
            sv = false
            sa = false
            if (bAuthSign) {
                bAuthSign = false
            }
            if (bVisSign) {
                bVisSign = false
            }
        }
        sign_dialog.show()
    }

    inner class Signature(context: Context?) : View(context) {
        private val paint = Paint()
        private val path = Path()
        private var lastTouchX = 0f
        private var lastTouchY = 0f
        private val dirtyRect: RectF = RectF()
        fun save(v: View?) {
            if (bVisSign) {
                Log.d("TAG", "bvsign")
                Log.v("tag", "Width: " + v!!.width)
                Log.v("tag", "Height: " + v.height)
                if (bitmap1 == null) {
                    bitmap1 = Bitmap.createBitmap(
                        mContent.width,
                        mContent.height,
                        Bitmap.Config.RGB_565
                    )
                }
                val canvas = Canvas(bitmap1!!)
                v.draw(canvas)
            }
            if (bAuthSign) {
                Log.d("TAG", "bAuthsign")
                if (bitmap2 == null) {
                    bitmap2 = Bitmap.createBitmap(
                        mContent.width, mContent.height,
                        Bitmap.Config.RGB_565
                    )
                }
                val canvas2 = Canvas(bitmap2!!)
                v!!.draw(canvas2)
            }
        }

        fun clear() {
            path.reset()
            invalidate()
        }

        override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX: Float = event.x
            val eventY: Float = event.y
            mGetSign.isEnabled = true
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }
                MotionEvent.ACTION_MOVE, MotionEvent.ACTION_UP -> {
                    resetDirtyRect(eventX, eventY)
                    val historySize: Int = event.historySize
                    var i = 0
                    while (i < historySize) {
                        val historicalX: Float = event.getHistoricalX(i)
                        val historicalY: Float = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                        i++
                    }
                    path.lineTo(eventX, eventY)
                }
                else -> {
                    debug("Ignored touch event: $event")
                    return false
                }
            }
            invalidate()
            lastTouchX = eventX
            lastTouchY = eventY
            return true
        }

        private fun debug(string: String) {
            Log.v("log_tag", string)
        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }
            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = min(lastTouchX, eventX)
            dirtyRect.right = max(lastTouchX, eventX)
            dirtyRect.top = min(lastTouchY, eventY)
            dirtyRect.bottom = max(lastTouchY, eventY)
        }

        init {
            paint.isAntiAlias = true
            paint.color = Color.BLACK
            paint.style = Paint.Style.STROKE
            paint.strokeJoin = Paint.Join.ROUND
            paint.strokeWidth = Companion.STROKE_WIDTH
        }

    }
    companion object {
        const val STROKE_WIDTH = 5f
    }
}